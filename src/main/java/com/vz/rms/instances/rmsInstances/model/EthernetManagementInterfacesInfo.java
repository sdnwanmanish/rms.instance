package com.vz.rms.instances.rmsInstances.model;

public class EthernetManagementInterfacesInfo {
	
	private String ethernetIndentifireLabel;
	private String connectionType;
	
	public String getEthernetIndentifireLabel() {
		return ethernetIndentifireLabel;
	}
	public void setEthernetIndentifireLabel(String ethernetIndentifireLabel) {
		this.ethernetIndentifireLabel = ethernetIndentifireLabel;
	}
	public String getConnectionType() {
		return connectionType;
	}
	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}
	
	

}
