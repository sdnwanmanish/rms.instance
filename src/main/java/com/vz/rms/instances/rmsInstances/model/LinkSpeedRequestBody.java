package com.vz.rms.instances.rmsInstances.model;

public class LinkSpeedRequestBody {

	
	private int link_speed;

	public int getLink_speed() {
		return link_speed;
	}

	public void setLink_speed(int link_speed) {
		this.link_speed = link_speed;
	}
	
}
