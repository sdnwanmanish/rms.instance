package com.vz.rms.instances.rmsInstances.model;

import java.util.List;

public class RMSRequestBody {

	private String name;//
	private String datapath_type;//
	private String datapath_id;//
	private String fb_ip;//
	private String protocols;//
	private String lldp_msg_tx_hold;//
	private String lldp_msg_tx_interval;//
	private String lldp_reinit_delay;//
	private String lldp_tx_delay;//
	private boolean global_lldp_enable;//
	private String ssl_name;//
	private String private_key;//
	private String certificate;//
	private int counter_query_interval;//
	private String ca_cert;//
	private String bootstrap_ca_cert;//
	private int sfp_query_interval;//
	private boolean sfp_enable;//
	private boolean alarm_enable;//
	private boolean counter_enable;//
	private boolean hwflow_enable;//
	private int hwflow_query_interval;//
	private int alarm_high_temp;//
	private int alarm_low_temp;//
	private List<String>arp_subnet;
	private List<String>nd_subnet;
	private List<Port>ports;//
	private List<LLDPInterfaceSettings>lldp_interface_settngs;//
	private List<Controller>controllers;//

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDatapath_type() {
		return datapath_type;
	}
	public void setDatapath_type(String datapath_type) {
		this.datapath_type = datapath_type;
	}
	public String getDatapath_id() {
		return datapath_id;
	}
	public void setDatapath_id(String datapath_id) {
		this.datapath_id = datapath_id;
	}
	public String getFb_ip() {
		return fb_ip;
	}
	public void setFb_ip(String fb_ip) {
		this.fb_ip = fb_ip;
	}
	public String getProtocols() {
		return protocols;
	}
	public void setProtocols(String protocols) {
		this.protocols = protocols;
	}
	public String getLldp_msg_tx_hold() {
		return lldp_msg_tx_hold;
	}
	public void setLldp_msg_tx_hold(String lldp_msg_tx_hold) {
		this.lldp_msg_tx_hold = lldp_msg_tx_hold;
	}
	public String getLldp_msg_tx_interval() {
		return lldp_msg_tx_interval;
	}
	public void setLldp_msg_tx_interval(String lldp_msg_tx_interval) {
		this.lldp_msg_tx_interval = lldp_msg_tx_interval;
	}
	public String getLldp_reinit_delay() {
		return lldp_reinit_delay;
	}
	public void setLldp_reinit_delay(String lldp_reinit_delay) {
		this.lldp_reinit_delay = lldp_reinit_delay;
	}
	public String getLldp_tx_delay() {
		return lldp_tx_delay;
	}
	public void setLldp_tx_delay(String lldp_tx_delay) {
		this.lldp_tx_delay = lldp_tx_delay;
	}
	public boolean isGlobal_lldp_enable() {
		return global_lldp_enable;
	}
	public void setGlobal_lldp_enable(boolean global_lldp_enable) {
		this.global_lldp_enable = global_lldp_enable;
	}
	public String getSsl_name() {
		return ssl_name;
	}
	public void setSsl_name(String ssl_name) {
		this.ssl_name = ssl_name;
	}
	public String getPrivate_key() {
		return private_key;
	}
	public void setPrivate_key(String private_key) {
		this.private_key = private_key;
	}
	public String getCertificate() {
		return certificate;
	}
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	public int getCounter_query_interval() {
		return counter_query_interval;
	}
	public void setCounter_query_interval(int counter_query_interval) {
		this.counter_query_interval = counter_query_interval;
	}
	public String getCa_cert() {
		return ca_cert;
	}
	public void setCa_cert(String ca_cert) {
		this.ca_cert = ca_cert;
	}
	public String getBootstrap_ca_cert() {
		return bootstrap_ca_cert;
	}
	public void setBootstrap_ca_cert(String bootstrap_ca_cert) {
		this.bootstrap_ca_cert = bootstrap_ca_cert;
	}
	public int getSfp_query_interval() {
		return sfp_query_interval;
	}
	public void setSfp_query_interval(int sfp_query_interval) {
		this.sfp_query_interval = sfp_query_interval;
	}
	public boolean isSfp_enable() {
		return sfp_enable;
	}
	public void setSfp_enable(boolean sfp_enable) {
		this.sfp_enable = sfp_enable;
	}
	public boolean isAlarm_enable() {
		return alarm_enable;
	}
	public void setAlarm_enable(boolean alarm_enable) {
		this.alarm_enable = alarm_enable;
	}
	public boolean isCounter_enable() {
		return counter_enable;
	}
	public void setCounter_enable(boolean counter_enable) {
		this.counter_enable = counter_enable;
	}
	public boolean isHwflow_enable() {
		return hwflow_enable;
	}
	public void setHwflow_enable(boolean hwflow_enable) {
		this.hwflow_enable = hwflow_enable;
	}
	public int getHwflow_query_interval() {
		return hwflow_query_interval;
	}
	public void setHwflow_query_interval(int hwflow_query_interval) {
		this.hwflow_query_interval = hwflow_query_interval;
	}
	public int getAlarm_high_temp() {
		return alarm_high_temp;
	}
	public void setAlarm_high_temp(int alarm_high_temp) {
		this.alarm_high_temp = alarm_high_temp;
	}
	public int getAlarm_low_temp() {
		return alarm_low_temp;
	}
	public void setAlarm_low_temp(int alarm_low_temp) {
		this.alarm_low_temp = alarm_low_temp;
	}	
	public List<String> getArp_subnet() {
		return arp_subnet;
	}
	public void setArp_subnet(List<String> arp_subnet) {
		this.arp_subnet = arp_subnet;
	}
	public List<String> getNd_subnet() {
		return nd_subnet;
	}
	public void setNd_subnet(List<String> nd_subnet) {
		this.nd_subnet = nd_subnet;
	}
	public List<Port> getPorts() {
		return ports;
	}
	public void setPorts(List<Port> ports) {
		this.ports = ports;
	}
	public List<LLDPInterfaceSettings> getLldp_interface_settngs() {
		return lldp_interface_settngs;
	}
	public void setLldp_interface_settngs(List<LLDPInterfaceSettings> lldp_interface_settngs) {
		this.lldp_interface_settngs = lldp_interface_settngs;
	}
	public List<Controller> getControllers() {
		return controllers;
	}
	public void setControllers(List<Controller> controllers) {
		this.controllers = controllers;
	}

	
	
	
	
}
