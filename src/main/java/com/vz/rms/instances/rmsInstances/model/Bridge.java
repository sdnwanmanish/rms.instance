package com.vz.rms.instances.rmsInstances.model;

import java.io.Serializable;



import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Bridge implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String datapath_type;
	private String datapath_id;
	private String protocols;
	private String fb_ip;
	
	 
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDatapath_type() {
		return datapath_type;
	}
	public void setDatapath_type(String datapath_type) {
		this.datapath_type = datapath_type;
	}	
	public String getDatapath_id() {
		return datapath_id;
	}
	public void setDatapath_id(String datapath_id) {
		this.datapath_id = datapath_id;
	}
	public String getProtocols() {
		return protocols;
	}
	public void setProtocols(String protocols) {
		this.protocols = protocols;
	}
	public String getFb_ip() {
		return fb_ip;
	}
	public void setFb_ip(String fb_ip) {
		this.fb_ip = fb_ip;
	}
	
	

}
