package com.vz.rms.instances.rmsInstances.config;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
@Configuration
@EnableAutoConfiguration 
public class RedisConfiguration {
	
	@Value("${rms.datasource.redisHostName}")
	private String hostName;
	
	@Value("${rms.datasource.redisPortNo}")
	private Integer port;
	
	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		System.out.println(hostName+"***********************Configuration Start**************************"+port);		
		JedisConnectionFactory jedisConFactory = new JedisConnectionFactory();
	    jedisConFactory.setHostName(hostName);
	    jedisConFactory.setPort(port);
	    //jedisConFactory.setPassword("");	   
	    return jedisConFactory;
	}
	 
	@Bean
	public RedisTemplate<String, List<Object>> listRedisTemplate() {
	    RedisTemplate<String, List<Object>> template = new RedisTemplate<String, List<Object>>();
	    template.setConnectionFactory(jedisConnectionFactory());
	    return template;
	}
	
	@Bean
	public RedisTemplate<String, Map<Object,Object>> redisTemplate() {
	    RedisTemplate<String, Map<Object,Object>> template = new RedisTemplate<String, Map<Object,Object>>();
	    template.setConnectionFactory(jedisConnectionFactory());
	    return template;
	}
	
	/*@Bean
	public RedisTemplate<String, Object> redisTemplateIP() {
	    RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
	    template.setConnectionFactory(jedisConnectionFactory());
	    return template;
	}*/


}
