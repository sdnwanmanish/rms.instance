package com.vz.rms.instances.rmsInstances.model;

public class FbOsTemplateRequestBody {
	
	private TemplateInfo templateInfo;
	private String osiLable;
	private String manufacturer;
	private String version;
	private String imageFileName;
	private String orderablePartNo;
	private String desc;
	private String materialId;
	private String openFlowProtocol;
	private String linuxKernalVersion;
	private String linuxDisPage;
	private OperatingSystemConfiguration operatingSystemConfiguration;
	
	public TemplateInfo getTemplateInfo() {
		return templateInfo;
	}
	public void setTemplateInfo(TemplateInfo templateInfo) {
		this.templateInfo = templateInfo;
	}
	public String getOsiLable() {
		return osiLable;
	}
	public void setOsiLable(String osiLable) {
		this.osiLable = osiLable;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getImageFileName() {
		return imageFileName;
	}
	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}
	public String getOrderablePartNo() {
		return orderablePartNo;
	}
	public void setOrderablePartNo(String orderablePartNo) {
		this.orderablePartNo = orderablePartNo;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getMaterialId() {
		return materialId;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	public String getOpenFlowProtocol() {
		return openFlowProtocol;
	}
	public void setOpenFlowProtocol(String openFlowProtocol) {
		this.openFlowProtocol = openFlowProtocol;
	}
	public String getLinuxKernalVersion() {
		return linuxKernalVersion;
	}
	public void setLinuxKernalVersion(String linuxKernalVersion) {
		this.linuxKernalVersion = linuxKernalVersion;
	}
	public String getLinuxDisPage() {
		return linuxDisPage;
	}
	public void setLinuxDisPage(String linuxDisPage) {
		this.linuxDisPage = linuxDisPage;
	}
	public OperatingSystemConfiguration getOperatingSystemConfiguration() {
		return operatingSystemConfiguration;
	}
	public void setOperatingSystemConfiguration(OperatingSystemConfiguration operatingSystemConfiguration) {
		this.operatingSystemConfiguration = operatingSystemConfiguration;
	}
	
	

}
