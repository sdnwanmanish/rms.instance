package com.vz.rms.instances.rmsInstances.model;

public class FbTransceiverTemplateRequestBody {
	
	private TemplateInfo templateInfo;
	private String transceiverType;
	private String manufacturer;
	private String orderablePartNo;
	private String description;
	private String supplier;
	private String supplierPartNo;	
	private String clei;
	private String materialId;
	public TemplateInfo getTemplateInfo() {
		return templateInfo;
	}
	public void setTemplateInfo(TemplateInfo templateInfo) {
		this.templateInfo = templateInfo;
	}
	public String getTransceiverType() {
		return transceiverType;
	}
	public void setTransceiverType(String transceiverType) {
		this.transceiverType = transceiverType;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getOrderablePartNo() {
		return orderablePartNo;
	}
	public void setOrderablePartNo(String orderablePartNo) {
		this.orderablePartNo = orderablePartNo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getSupplier() {
		return supplier;
	}
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}
	public String getSupplierPartNo() {
		return supplierPartNo;
	}
	public void setSupplierPartNo(String supplierPartNo) {
		this.supplierPartNo = supplierPartNo;
	}
	public String getClei() {
		return clei;
	}
	public void setClei(String clei) {
		this.clei = clei;
	}
	public String getMaterialId() {
		return materialId;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	
	
	

}
