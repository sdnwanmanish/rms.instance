package com.vz.rms.instances.rmsInstances.model;

public class BridgeInsertRequestBody {
	
	private String name;
	private String controller_ip;
	private String of_port;
	private String connect_protocol;
	private String fb_ip;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getController_ip() {
		return controller_ip;
	}
	public void setController_ip(String controller_ip) {
		this.controller_ip = controller_ip;
	}
	public String getOf_port() {
		return of_port;
	}
	public void setOf_port(String of_port) {
		this.of_port = of_port;
	}
	public String getConnect_protocol() {
		return connect_protocol;
	}
	public void setConnect_protocol(String connect_protocol) {
		this.connect_protocol = connect_protocol;
	}
	public String getFb_ip() {
		return fb_ip;
	}
	public void setFb_ip(String fb_ip) {
		this.fb_ip = fb_ip;
	}
	
	

}
