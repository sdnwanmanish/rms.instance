package com.vz.rms.instances.rmsInstances.model;

public class DataPlaneInterfaceInfo {
	
	private String interfaceType;
	private String interfaceLabel;
	private String connector1;
	private String connector2;
	private String connector3;
	private String connector4;
	
	public String getInterfaceType() {
		return interfaceType;
	}
	public void setInterfaceType(String interfaceType) {
		this.interfaceType = interfaceType;
	}
	public String getInterfaceLabel() {
		return interfaceLabel;
	}
	public void setInterfaceLabel(String interfaceLabel) {
		this.interfaceLabel = interfaceLabel;
	}
	public String getConnector1() {
		return connector1;
	}
	public void setConnector1(String connector1) {
		this.connector1 = connector1;
	}
	public String getConnector2() {
		return connector2;
	}
	public void setConnector2(String connector2) {
		this.connector2 = connector2;
	}
	public String getConnector3() {
		return connector3;
	}
	public void setConnector3(String connector3) {
		this.connector3 = connector3;
	}
	public String getConnector4() {
		return connector4;
	}
	public void setConnector4(String connector4) {
		this.connector4 = connector4;
	}
	
	

}
