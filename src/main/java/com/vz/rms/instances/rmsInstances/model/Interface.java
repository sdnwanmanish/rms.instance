package com.vz.rms.instances.rmsInstances.model;

public class Interface {
	
	private String name;
	private String type;
	private String link_status;
	private String admin_status;
	private Integer link_speed;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getLink_status() {
		return link_status;
	}
	public void setLink_status(String link_status) {
		this.link_status = link_status;
	}
	public String getAdmin_status() {
		return admin_status;
	}
	public void setAdmin_status(String admin_status) {
		this.admin_status = admin_status;
	}
	public Integer getLink_speed() {
		return link_speed;
	}
	public void setLink_speed(Integer link_speed) {
		this.link_speed = link_speed;
	}
	
	

}
