package com.vz.rms.instances.rmsInstances.repository;


import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import com.vz.rms.instances.rmsInstances.model.IpDetails;


@Repository
public class RepositoryImplClass implements RepositoryClass {
	
	//private static final String KEY = "FB";	
	
	private RedisTemplate<String, Object> redisTemplate;
	
	//private RedisTemplate<String, Object> redisTemplateIp;
	private ValueOperations<String, Object> TokenOps;
	
    private HashOperations hashOps;    
    private RedisTemplate<String, Map<Object,Object>> listRedisTemplate;
    
    @Autowired
   public RepositoryImplClass(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
        this.listRedisTemplate = redisTemplate;
    } 
  
    @PostConstruct
    private void init() {
        hashOps = redisTemplate.opsForHash();
        hashOps = listRedisTemplate.opsForHash();
        TokenOps = redisTemplate.opsForValue();
    }
	@Override
	public void saveInstances(String key,String uuid,String fb_name) {		
		hashOps.put(key, fb_name, uuid);
		
	}
	@Override
	public void updateInstances(String key, String uuid, String fb_name) {
		hashOps.put(key, fb_name, uuid);
		
	}
	@Override
	public String findInstances(String key, String fb_name) {
		
		return (String) hashOps.get(key, fb_name);
	}
	@Override
	public Map<String, String> findAllInstances(String key) {
		
		return hashOps.entries(key);
	}
	@Override
	public void deleteInstances(String key, String fb_name) {
		
		 hashOps.delete(key, fb_name);		
	}
	@Override
	public void save(String key,String id, Map<String,Object> listObject) {
		hashOps.put(key, id, listObject);
		
	}
	@Override
	public Map<String,Object> getAllPort(String key,String id) {
		// TODO Auto-generated method stub
		Map<String,Object> listPort = (Map<String,Object>) hashOps.get(key,id);
		return listPort;
	}
	@Override
	public Map<String,Object> findAllFbName(String key) {
		// TODO Auto-generated method stub
		return  hashOps.entries(key);
		
		
	}
	@Override
	public Object getLoginInfo(String key) {
		return  TokenOps.get(key);
	}
	@Override
	public Object get(String key, String id) {
		return  hashOps.get(key,id);
	}
	
	@Override
	public void saveObject(String key,String id, Object object) {
		hashOps.put(key, id, object);
		
	}
	
}
