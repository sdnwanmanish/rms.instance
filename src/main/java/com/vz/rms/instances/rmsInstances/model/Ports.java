package com.vz.rms.instances.rmsInstances.model;

import java.util.List;

public class Ports {

	
	private String name;
	private String vlan_mode;
	private String type;
	private String speed;
	private boolean is_dac;
	private List<Integer> trunks;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVlan_mode() {
		return vlan_mode;
	}
	public void setVlan_mode(String vlan_mode) {
		this.vlan_mode = vlan_mode;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSpeed() {
		return speed;
	}
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	public boolean isIs_dac() {
		return is_dac;
	}
	public void setIs_dac(boolean is_dac) {
		this.is_dac = is_dac;
	}
	public List<Integer> getTrunks() {
		return trunks;
	}
	public void setTrunks(List<Integer> trunks) {
		this.trunks = trunks;
	}

	
}
