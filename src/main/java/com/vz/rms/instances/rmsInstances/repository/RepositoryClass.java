package com.vz.rms.instances.rmsInstances.repository;



import java.util.Map;

public interface RepositoryClass {

	public void saveInstances(String key, String uuid, String fb_name);
	
	public void save(String key,String id, Map<String,Object> list);
	
	public Map<String,Object> getAllPort(String key,String id);

	public void updateInstances(String key, String uuid, String fb_name);

	public String findInstances(String key, String fb_name);

	public Map<String, String> findAllInstances(String key);

	public void deleteInstances(String key, String fb_name);
	
	public Map<String,Object> findAllFbName(String key);
	
	public Object getLoginInfo(String key);
	
	public Object get(String key, String id);
	
	public void saveObject(String key,String id, Object object);

}
