package com.vz.rms.instances.rmsInstances.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.vz.rms.instances.rmsInstances.model.Bridge;
import com.vz.rms.instances.rmsInstances.model.ConfigureARPAndND;
import com.vz.rms.instances.rmsInstances.model.Controller;
import com.vz.rms.instances.rmsInstances.model.LLDP;
import com.vz.rms.instances.rmsInstances.model.LLDPPerInterfaceRequestBody;
import com.vz.rms.instances.rmsInstances.model.LinkSpeedRequestBody;
import com.vz.rms.instances.rmsInstances.model.PollingFrequencyRequestBody;
import com.vz.rms.instances.rmsInstances.model.Port;
import com.vz.rms.instances.rmsInstances.model.PortStatus;
import com.vz.rms.instances.rmsInstances.model.SSLRequestBody;
import com.vz.rms.instances.rmsInstances.repository.RepositoryImplClass;
import com.vz.rms.instances.rmsInstances.util.Utils;
import io.swagger.annotations.ApiOperation;
import com.vz.rms.instances.rmsInstances.model.PortDetails;
import org.apache.el.parser.ParseException;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/rms")
public class RMSController {

	@Autowired
	Utils util;

	@Autowired
	RepositoryImplClass repositoryImplClass;

	@Value("${rms.serverapi.ipaddress}")
	private String serverAddress;

	@Value("${rms.serverapi.port}")
	private String port;

	private final String rmsUrl = "/oneoss-rms/rms/v1/";

	private final String ADDPORTKEY = "PORT";	

	private static final Logger logger = LoggerFactory.getLogger(RMSController.class);
	//HashMap<String,String> ipDetails = (HashMap<String,String>) repositoryImplClass.getLoginInfo("IpDetails");
	@RequestMapping(value = "{fbname}/add-bridge", method = RequestMethod.POST, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Creates the bridge with controller specific information ", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Created"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public @ResponseBody Map<String,String> addBridge(@PathVariable String fbname,
			@RequestBody Bridge bridge) throws ParseException, JSONException, InterruptedException, ExecutionException, TimeoutException, IOException {
		Map<String,String> message = new HashMap<String,String>();		
		message = util.saveBridge(fbname, bridge);						
		return message;
	}

	@RequestMapping(value = "{fbname}/listBridge", method = RequestMethod.GET)
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Creates the bridge with controller specific information ", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Created"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public @ResponseBody List<Bridge> getBridge(@PathVariable String fbname)
			throws ParseException, JSONException {

		//logger.info("**GetBridge**");
		//logger.info("FbName:" + fbname);
		List<Bridge> listBridge = new ArrayList<Bridge>();
		Map<String, Object> mapBridge = util.getBridge(fbname);
		if (mapBridge == null) {
			return listBridge;
		}
		for (Map.Entry<String, Object> entry : mapBridge.entrySet()) {
			Bridge bridge = (Bridge) entry.getValue();
			listBridge.add(bridge);
		}

		// Map<String, Object> listFb =
		// repositoryImplClass.findAllFbName("BRIDGE");
		// logger.info("Size:"+listFb.size());
		return listBridge;
	}
	
	@RequestMapping(value = "{fbname}/delete-bridge/{brname}", method = RequestMethod.DELETE, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Deletes the FB and associated interface", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Deleted FB "),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public @ResponseBody Map<String,String> deleteBridge(@PathVariable String fbname,
			@PathVariable String brname) throws ParseException, InterruptedException, ExecutionException, TimeoutException {		
			Map<String,String> message  = new HashMap<String,String>();		
			message = util.deleteBridge(fbname, brname).get(20,TimeUnit.SECONDS);
			return message;		
	}

	@RequestMapping(value = "{fbname}/set-controller", method = RequestMethod.POST, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Configures the bridge with controller specific information such target IP", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful configured the Bridge"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> setController(@PathVariable String fbname,
			@RequestBody Controller controller) throws ParseException,
			JSONException {
		logger.info("Set Controller");
		Map<String,String> message  = new HashMap<String,String>();		
		message = util.saveController(fbname, controller);
		return message;
	}

	@RequestMapping(value = "{fbname}/set-lldp", method = RequestMethod.POST, produces = "application/json")
	@ApiOperation(value = "Configures the bridge with lldp specific information such lldp enable", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful configured the bridge with lldp"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> setLLDP(@PathVariable String fbname, @RequestBody LLDP lldp)
			throws ParseException {

		logger.info("**setLLDP**");
		Map<String,String> message  = new HashMap<String,String>();
		Object object = repositoryImplClass.get("LLDP", fbname);
		if(object!=null){
			message.put("type","failure");
			message.put("message","LLDp al ready exist!");
			return message;
		}
		repositoryImplClass.saveObject("LLDP", fbname, lldp);
		logger.info("fbnName:"+fbname+",Added sucessfully!");
		message.put("type","success");
		message.put("message","LLDP added sucessfully!");		
		return message;
	}

	@RequestMapping(value = "{fbname}/port/add", method = RequestMethod.POST, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Creates the port and the corresponding interface", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful port created"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public @ResponseBody Map<String,String> addPort(@PathVariable String fbname,
			@RequestBody Port port) throws ParseException, JSONException {
		logger.info("**AddPort**");
		PortStatus portStatus = new PortStatus();
		portStatus.setPort(port);
		portStatus.setStatus("true");
		Map<String,String> message = new HashMap<String,String>();
		Boolean status = false;
		Map<String, Object> listPort = repositoryImplClass.getAllPort(
				ADDPORTKEY, fbname);
		if (listPort == null)
			listPort = new HashMap<String, Object>();
		status = listPort.containsKey(port.getName());
		if (status) {
			message.put("type", "failure");
			message.put("message", "Port already exists!");
			return message;
		}
		listPort.put(port.getName(), portStatus);
		repositoryImplClass.save(ADDPORTKEY, fbname, listPort);
		logger.info("Port added sucessfully!");
		message.put("type", "success");
		message.put("message", "Port added sucessfully!");
		return message;
	}

	@RequestMapping(value = "{fbname}/port/find", method = RequestMethod.POST, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Creates the port and the corresponding interface", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful port created"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public @ResponseBody List<PortDetails> PortDetails(
			@PathVariable String fbname) throws ParseException, JSONException {
		
		
		List<PortDetails> listResult = new ArrayList<PortDetails>();
		//Map<String, String> details = new HashMap<String, String>();
		Map<String, Object> listPort = repositoryImplClass.getAllPort(
				ADDPORTKEY, fbname);
		if (listPort == null) {			
			return listResult;
		}
		for (Map.Entry<String, Object> entry : listPort.entrySet()) {
			//String key = entry.getKey();	
			PortDetails portDetails = new PortDetails();
			PortStatus portStatus = (PortStatus) entry.getValue();
			Port port = portStatus.getPort();
			portDetails.setName(port.getName());			
			logger.info("---->"+port.getName());
			portDetails.setIsFree(portStatus.getStatus());
			listResult.add(portDetails);
			//details.put(key, port.getStatus());
		}
		return listResult;
	}

	@RequestMapping(value = "{fbname}/{portName}/{status}", method = RequestMethod.POST)
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Change the status of port", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful changed the status"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public @ResponseBody String PortChangeStatus(@PathVariable String fbname,
			@PathVariable String portName, @PathVariable String status)
			throws ParseException, JSONException, InterruptedException, ExecutionException, TimeoutException {

		String response = util.changeStatusOfPortInRms(fbname, portName, status).get(20, TimeUnit.SECONDS);
		if(response.contains("error"))
			return "failure";
		Map<String, Object> listPort = repositoryImplClass.getAllPort(
				ADDPORTKEY, fbname);
		for (Map.Entry<String, Object> entry : listPort.entrySet()) {
			String key = entry.getKey();
			PortStatus portStatus = (PortStatus) entry.getValue();
			Port port = portStatus.getPort();
			if (port.getName().equals(portName)) {
				portStatus.setStatus(status);
				listPort.put(key, portStatus);
			}
		}
		repositoryImplClass.save(ADDPORTKEY, fbname, listPort);			
		return "success";
	}
	@RequestMapping(value = "{fbname}/list-ports", method = RequestMethod.GET, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Configuration settings for “Polling Frequencies” and “alarm” configuration", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Configured the Polling Frequencies "),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public @ResponseBody List<Port> listPort(@PathVariable String fbname)
			throws ParseException {

		List<Port> portList = new ArrayList<Port>();		
		Map<String, Object> listPort = repositoryImplClass.getAllPort(
				ADDPORTKEY, fbname);
		if (listPort == null) {			
			return portList;
		}
		for (Map.Entry<String, Object> entry : listPort.entrySet()) {						
			PortStatus portStatus = (PortStatus) entry.getValue();
			Port port = portStatus.getPort();
			portList.add(port);			
		}
		return portList;	
	}
	

	@RequestMapping(value = "{fbname}/{portname}/configure/status/{status}", method = RequestMethod.PUT, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Activate/Deactivate interface by given inputs", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Activate/Deactivate "),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String activateInterface(@PathVariable String fbname,
			@PathVariable String portname, @PathVariable String status)
			throws ParseException {
		logger.info("**ActivateInterface**");
		RestTemplate restTemplate = new RestTemplate();

		String url = serverAddress + ":" + port + rmsUrl + fbname + "/"
				+ portname + "/configure/status/" + status;
		logger.info("Url:" + url);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(
				"Activate Interface", headers);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url,
				HttpMethod.PUT, entity, String.class);
		logger.info("--->" + responseEntity.getBody());
		// ResponseEntity<String> response =
		// restTemplate.exchange(url,HttpMethod.PUT,"",String.class);
		return responseEntity.getBody();
	}

	@RequestMapping(value = "{fbname}/{portname}/delete", method = RequestMethod.DELETE, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Deletes the port and associated interface", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Deleted "),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public @ResponseBody Map<String,String> deletePort(@PathVariable String fbname,
			@PathVariable String portname) throws ParseException, InterruptedException, ExecutionException, TimeoutException, JSONException {
		logger.info("Delete Port!");
		Map<String,String> message = new HashMap<String,String>();		
		message = util.deletePort(fbname, portname).get(20,TimeUnit.SECONDS);				
		return message;
		
	}
	@RequestMapping(value = "{fbname}/{portname}/configure/link-speed", method = RequestMethod.POST, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Configure the speed for the given port", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Configured the speed "),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String linkSpeed(@PathVariable String fbname,
			@PathVariable String portname,
			@RequestBody LinkSpeedRequestBody linkspeed) throws ParseException {
		logger.info("**Put**");
		RestTemplate restTemplate = new RestTemplate();

		// HttpEntity<String> requestUpdate = new HttpEntity<String>();
		String url = serverAddress + ":" + port + rmsUrl + fbname + "/"
				+ portname + "/configure/link-speed";
		logger.info("Url" + url);
		// restTemplate.delete(url);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(
				"Configure link-speed", headers);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url,
				HttpMethod.PUT, entity, String.class, linkspeed);
		logger.info("--->" + responseEntity.getBody());
		;
		logger.info("**LinkSpeed**");
		return "success";
	}

	@RequestMapping(value = "{fbname}/configure/polling-frequency", method = RequestMethod.POST, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Configuration settings for “Polling Frequencies” and “alarm” configuration", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Configured the Polling Frequencies "),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> pollFrequency(@PathVariable String fbname,
			@RequestBody PollingFrequencyRequestBody pollingfrequency)
			throws ParseException {
		logger.info("**Polling Frequence**");
		Map<String,String> message  = new HashMap<String,String>();
		Object object = repositoryImplClass.get("POLLING", fbname);
		if(object!=null){
			message.put("type","failure");
			message.put("message","Polling frequency  already exist!");
			return message;
		}
		repositoryImplClass.saveObject("POLLING", fbname, pollingfrequency);
		logger.info("fbnName:"+fbname+",Added sucessfully!");
		message.put("type","success");
		message.put("message","Polling frequency added sucessfully!");		
		return message;
	}



	@RequestMapping(value = "{fbname}/configure/neighbor-discovery", method = RequestMethod.POST, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Configuration settings for “Polling Frequencies” and “alarm” configuration", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Configured the Polling Frequencies "),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> configureARPAndND(@PathVariable String fbname,
			@RequestBody ConfigureARPAndND configureARPAndND)
			throws ParseException {
		logger.info("**Configure ARP And ND**");
		Map<String,String> message  = new HashMap<String,String>();
		Object object = repositoryImplClass.get("ARPND", fbname);
		if(object!=null){
			message.put("type","failure");
			message.put("message","ARP And ND  already exist!");
			return message;
		}
		repositoryImplClass.saveObject("ARPND", fbname, configureARPAndND);
		logger.info("fbnName:"+fbname+",Added sucessfully!");
		message.put("type","success");
		message.put("message","ARP And ND added sucessfully!");		
		return message;		
	}

	@RequestMapping(value = "{fbname}/set-lldp/perint", method = RequestMethod.POST, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Configures the Forwarding Box with LLDP at an interface level", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Configures the Forwarding Box with LLDP "),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> lldpPerInt(@PathVariable String fbname,
			@RequestBody LLDPPerInterfaceRequestBody lldpint)
			throws ParseException {
		logger.info("**lldpPerInt**");
		Map<String,String> message  = new HashMap<String,String>();
		Object object = repositoryImplClass.get("PERINT", fbname);
		if(object!=null){
			message.put("type","failure");
			message.put("message","Perint  already exist!");
			return message;
		}
		repositoryImplClass.saveObject("PERINT", fbname, lldpint);
		logger.info("fbnName:"+fbname+",Added sucessfully!");
		message.put("type","success");
		message.put("message","PerInt added sucessfully!");		
		return message;
		
	}
	
	@RequestMapping(value = "{fbname}/ssl", method = RequestMethod.POST, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "This API used to configure the SSL", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful configured the SSl"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> setSSl(@PathVariable String fbname,@RequestBody SSLRequestBody ssl) throws ParseException {
		logger.info("**setSSl**");
		logger.info("**Configure ARP And ND**");
		Map<String,String> message  = new HashMap<String,String>();
		Object object = repositoryImplClass.get("SSL", fbname);
		if(object!=null){
			message.put("type","failure");
			message.put("message","SSL  already exist!");
			return message;
		}
		repositoryImplClass.saveObject("SSL", fbname, ssl);
		logger.info("SSL Added sucessfully!");
		message.put("type","success");
		message.put("message","SSL added sucessfully!");		
		return message;		
	}
	@RequestMapping(value = "{fbname}/composite", method = RequestMethod.POST, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "This API used to setup the Forwarding Box with datapath_type and datapath_id and ports", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful setup the FB"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public Map<String,String> fbSetup(@PathVariable String fbname) throws ParseException, InterruptedException, ExecutionException, TimeoutException, IOException, JSONException {
		logger.info("CompositeApi");
		Map<String,String> message  = new HashMap<String,String>();
		message = util.createComposite(fbname);
		return message;
	}

	/*@RequestMapping(value = "{fbname}/configure/neighbor-discovery", method = RequestMethod.PUT, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Configuration settings for the ARP and ICMP subnets", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Configured the ARP and ICMP subnets"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public @ResponseBody NeighborDiscoveryRequestBody neighborDiscovery(
			@PathVariable String fbname,
			@RequestBody NeighborDiscoveryRequestBody neighbordiscovery)
			throws ParseException {

		logger.info("**neighborDiscover**");
		return neighbordiscovery;
	}*/
/*
	@RequestMapping(value = "{fbname}/delete/neighbor-discovery/{ipsubnet}", method = RequestMethod.DELETE, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Deletes the requested subnet (arp/nd)", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Deleted the ARP and nd"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String neighborDiscoveryDelete(@PathVariable String fbname,
			@PathVariable String ipsubnet) throws ParseException {
		logger.info("**DeleteNeighbour**");
		return "success";
	}*/

/*	@RequestMapping(value = "{fbname}/list-neighbor-discovery", method = RequestMethod.GET, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Lists the available ARP and ND subnets", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Listed the ARP and nd"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String listNeighborDiscovery(@PathVariable String fbname)
			throws ParseException {
		logger.info("**listNeighbor**");
		TestRestTemplate restTemplate = new TestRestTemplate();
		String url = serverAddress + ":" + port + rmsUrl + fbname
				+ "/list-neighbor-discovery";
		String response = restTemplate.getForObject(url, String.class);
		logger.info("Response:" + response);

		return response;
	}*/

	/*@RequestMapping(value = "{fbname}/configure/tcam", method = RequestMethod.PUT, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Configures the TCAM configuration", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful configured the TCAM"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String configureTCAM(@PathVariable String fbname,
			@RequestBody TcamRequestBody tcam) throws ParseException {
		logger.info("**configure TCM**");
		TestRestTemplate restTemplate = new TestRestTemplate();

		// HttpEntity<String> requestUpdate = new HttpEntity<String>();
		String url = serverAddress + ":" + port + rmsUrl + fbname
				+ "/configure/tcam";
		logger.info("Url" + url);
		// restTemplate.delete(url);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>("Configure TCAM",
				headers);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url,
				HttpMethod.PUT, entity, String.class, tcam);
		logger.info("--->" + responseEntity.getBody());
		;
		logger.info("**LinkSpeed**");
		logger.info("**pollFrequency**");

		return "Success";
	}*/
/*
	@RequestMapping(value = "{fbname}/tcam/{tcamname}/delete", method = RequestMethod.GET, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Deletes the requested TCAM match mode", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful deleted the requested TCAM"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String deleteTCAM(@PathVariable String fbname,
			@PathVariable String tcamname) throws ParseException {
		logger.info("**deleteTCAM**");
		return "success";
	}

	@RequestMapping(value = "{fbname}/lldp/interface/{id}", method = RequestMethod.GET, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Returns All/current lldp status of the interface", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful returns all lldp status"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String getLLDPStatus(@PathVariable String fbname,
			@PathVariable String id) throws ParseException {
		logger.info("**LLDP status**");
		return "success";
	}*/


	/*@RequestMapping(value = "{fbname}/change-ip/{fbip}", method = RequestMethod.PUT, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Changes the existing fb entry with the new IP", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful changed the existing fb"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String changeFBIp(@PathVariable String fbname,
			@PathVariable String fbip) throws ParseException {
		logger.info("**changeFBIp**");
		logger.info("**configure TCM**");
		TestRestTemplate restTemplate = new TestRestTemplate();

		// HttpEntity<String> requestUpdate = new HttpEntity<String>();
		String url = serverAddress + ":" + port + rmsUrl + fbname
				+ "/change-ip/" + fbip;
		logger.info("Url" + url);
		// restTemplate.delete(url);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>("change FB IP",
				headers);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url,
				HttpMethod.PUT, entity, String.class);
		logger.info("--->" + responseEntity.getBody());
		;

		return "success";
	}*/

	

	/*@RequestMapping(value = "/{fb_name}/open-flow-count", method = RequestMethod.GET, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "This API used to configure the SSL", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful configured the SSl"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String openFlowCountServices(@PathVariable String fbname)
			throws ParseException {
		logger.info("**OpenFlow**");
		TestRestTemplate restTemplate = new TestRestTemplate();
		String url = "http://10.200.0.229:8090/oneoss-rms/rms/v1/esre/open-flow-count";
		String response = restTemplate.getForObject(url, String.class);
		logger.info("Response:" + response);
		return response;
	}*/

	

	/*@RequestMapping(value = "{fbname}/open-flow-count-match-mode/{match_mode_name}", method = RequestMethod.GET, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "This API used to setup the Forwarding Box with datapath_type and datapath_id and ports", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful setup the FB"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String TcamMatchMode(@PathVariable String fbname,
			@PathVariable String match_mode_name) throws ParseException {
		logger.info("**Get Flow HandlingMode**");
		TestRestTemplate restTemplate = new TestRestTemplate();
		String url = serverAddress + ":" + port + rmsUrl + fbname
				+ "/open-flow-count-match-mode/" + match_mode_name;
		String response = restTemplate.getForObject(url, String.class);
		logger.info("Response:" + response);
		return response;
	}

	@RequestMapping(value = "{fbname}/get-flowhandling-mode", method = RequestMethod.GET, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "This API used to setup the Forwarding Box with datapath_type and datapath_id and ports", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful setup the FB"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String getFlowHandlingMode(@PathVariable String fbname)
			throws ParseException {
		logger.info("**Get Flow HandlingMode**");
		TestRestTemplate restTemplate = new TestRestTemplate();
		String url = serverAddress + ":" + port + rmsUrl + fbname
				+ "/get-flowhandling-mode";
		String response = restTemplate.getForObject(url, String.class);
		logger.info("Response:" + response);
		return response;
	}

	@RequestMapping(value = "{fbname}/get-group-hash_fields", method = RequestMethod.GET, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "This API used to setup the Forwarding Box with datapath_type and datapath_id and ports", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful setup the FB"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String getGroupHashField(@PathVariable String fbname)
			throws ParseException {
		logger.info("**fbSetup**");
		TestRestTemplate restTemplate = new TestRestTemplate();
		String url = serverAddress + ":" + port + rmsUrl + fbname
				+ "/get-group-hash_fields";
		String response = restTemplate.getForObject(url, String.class);
		logger.info("Response:" + response);
		return response;
	}

	@RequestMapping(value = "{fbname}/configure/group-hash-fields/{grouphashfields}", method = RequestMethod.PUT, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Changes the existing fb entry with the new IP", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful changed the existing fb"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String groupHashField(@PathVariable String fbname,
			@PathVariable String grouphashfields) throws ParseException {
		logger.info("**changeFBIp**");
		logger.info("**configure TCM**");
		TestRestTemplate restTemplate = new TestRestTemplate();

		// HttpEntity<String> requestUpdate = new HttpEntity<String>();
		String url = serverAddress + ":" + port + rmsUrl + fbname
				+ "/configure/group-hash-fields/" + grouphashfields;
		logger.info("Url" + url);
		// restTemplate.delete(url);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>("Group hash Field",
				headers);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url,
				HttpMethod.PUT, entity, String.class);
		logger.info("--->" + responseEntity.getBody());
		;

		return "success";
	}

	@RequestMapping(value = "{fbname}/configure/flow-handle-mode/{flowhandlingmode}", method = RequestMethod.PUT, produces = "application/json")
	// @PerformanceMetrics(sla = 500)
	@ApiOperation(value = "Changes the existing fb entry with the new IP", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful changed the existing fb"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public String setFlowHandlingMode(@PathVariable String fbname,
			@PathVariable String flowhandlingmode) throws ParseException {
		logger.info("**changeFBIp**");
		logger.info("**configure TCM**");
		TestRestTemplate restTemplate = new TestRestTemplate();
		// HttpEntity<String> requestUpdate = new HttpEntity<String>();
		String url = serverAddress + ":" + port + rmsUrl + fbname
				+ "/configure/flow-handle-mode/" + flowhandlingmode;
		logger.info("Url" + url);
		// restTemplate.delete(url);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(
				"Flow Handling Mode!", headers);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url,
				HttpMethod.PUT, entity, String.class);
		logger.info("--->" + responseEntity.getBody());
		;

		return "success";
	}*/
}
