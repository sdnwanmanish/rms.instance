package com.vz.rms.instances.rmsInstances.model;

public class FbPowerSupplyRequestBody {
	
	private TemplateInfo templateInfo;
	private String manufacturer;
	private String orderablePartNo;
	private String description;
	private String clei;
	private String materialId;
	private String powerSupplyType;
	private String maxPowerRating;
	private String airFlow;
	public TemplateInfo getTemplateInfo() {
		return templateInfo;
	}
	public void setTemplateInfo(TemplateInfo templateInfo) {
		this.templateInfo = templateInfo;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getOrderablePartNo() {
		return orderablePartNo;
	}
	public void setOrderablePartNo(String orderablePartNo) {
		this.orderablePartNo = orderablePartNo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getClei() {
		return clei;
	}
	public void setClei(String clei) {
		this.clei = clei;
	}
	public String getMaterialId() {
		return materialId;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	public String getPowerSupplyType() {
		return powerSupplyType;
	}
	public void setPowerSupplyType(String powerSupplyType) {
		this.powerSupplyType = powerSupplyType;
	}
	public String getMaxPowerRating() {
		return maxPowerRating;
	}
	public void setMaxPowerRating(String maxPowerRating) {
		this.maxPowerRating = maxPowerRating;
	}
	public String getAirFlow() {
		return airFlow;
	}
	public void setAirFlow(String airFlow) {
		this.airFlow = airFlow;
	}
	

}
