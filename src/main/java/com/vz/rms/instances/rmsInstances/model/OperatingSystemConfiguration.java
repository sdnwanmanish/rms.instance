package com.vz.rms.instances.rmsInstances.model;

import java.io.Serializable;

public class OperatingSystemConfiguration implements Serializable{
	
	private String localArpResponse;
	private String localArpResponseCoverage;
	private String localIpv6ArpResponse;
	private String localIpv6ArpResponseCoverage;
	private String fb_sfp_interval;
	private String interfceFlowCounterInt;
	private String alarmHistory;
	private String alarmHighTemp;
	private String alarmLowTemp;
	public String getLocalArpResponse() {
		return localArpResponse;
	}
	public void setLocalArpResponse(String localArpResponse) {
		this.localArpResponse = localArpResponse;
	}
	public String getLocalArpResponseCoverage() {
		return localArpResponseCoverage;
	}
	public void setLocalArpResponseCoverage(String localArpResponseCoverage) {
		this.localArpResponseCoverage = localArpResponseCoverage;
	}
	public String getLocalIpv6ArpResponse() {
		return localIpv6ArpResponse;
	}
	public void setLocalIpv6ArpResponse(String localIpv6ArpResponse) {
		this.localIpv6ArpResponse = localIpv6ArpResponse;
	}
	public String getLocalIpv6ArpResponseCoverage() {
		return localIpv6ArpResponseCoverage;
	}
	public void setLocalIpv6ArpResponseCoverage(String localIpv6ArpResponseCoverage) {
		this.localIpv6ArpResponseCoverage = localIpv6ArpResponseCoverage;
	}
	public String getFb_sfp_interval() {
		return fb_sfp_interval;
	}
	public void setFb_sfp_interval(String fb_sfp_interval) {
		this.fb_sfp_interval = fb_sfp_interval;
	}
	public String getInterfceFlowCounterInt() {
		return interfceFlowCounterInt;
	}
	public void setInterfceFlowCounterInt(String interfceFlowCounterInt) {
		this.interfceFlowCounterInt = interfceFlowCounterInt;
	}
	public String getAlarmHistory() {
		return alarmHistory;
	}
	public void setAlarmHistory(String alarmHistory) {
		this.alarmHistory = alarmHistory;
	}
	public String getAlarmHighTemp() {
		return alarmHighTemp;
	}
	public void setAlarmHighTemp(String alarmHighTemp) {
		this.alarmHighTemp = alarmHighTemp;
	}
	public String getAlarmLowTemp() {
		return alarmLowTemp;
	}
	public void setAlarmLowTemp(String alarmLowTemp) {
		this.alarmLowTemp = alarmLowTemp;
	}

}
