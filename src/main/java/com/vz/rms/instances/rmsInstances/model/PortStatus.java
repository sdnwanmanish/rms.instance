package com.vz.rms.instances.rmsInstances.model;

import java.io.Serializable;

public class PortStatus implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Port port;
	private String status;
	
	public Port getPort() {
		return port;
	}
	public void setPort(Port port) {
		this.port = port;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
