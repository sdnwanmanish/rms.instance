package com.vz.rms.instances.rmsInstances.model;

import java.io.Serializable;

public class PollingFrequencyRequestBody implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int counter_query_interval;
	private int sfp_query_interval;
	private boolean sfp_enable;
	private boolean alarm_enable;
	private boolean counter_enable;
	private int alarm_high_temp;
	private int alarm_low_temp;
	private boolean hwflow_enable;
	private int hwflow_query_interval;
	public int getCounter_query_interval() {
		return counter_query_interval;
	}
	public void setCounter_query_interval(int counter_query_interval) {
		this.counter_query_interval = counter_query_interval;
	}
	public int getSfp_query_interval() {
		return sfp_query_interval;
	}
	public void setSfp_query_interval(int sfp_query_interval) {
		this.sfp_query_interval = sfp_query_interval;
	}
	public boolean isSfp_enable() {
		return sfp_enable;
	}
	public void setSfp_enable(boolean sfp_enable) {
		this.sfp_enable = sfp_enable;
	}
	public boolean isAlarm_enable() {
		return alarm_enable;
	}
	public void setAlarm_enable(boolean alarm_enable) {
		this.alarm_enable = alarm_enable;
	}
	public boolean isCounter_enable() {
		return counter_enable;
	}
	public void setCounter_enable(boolean counter_enable) {
		this.counter_enable = counter_enable;
	}
	public int getAlarm_high_temp() {
		return alarm_high_temp;
	}
	public void setAlarm_high_temp(int alarm_high_temp) {
		this.alarm_high_temp = alarm_high_temp;
	}
	public int getAlarm_low_temp() {
		return alarm_low_temp;
	}
	public void setAlarm_low_temp(int alarm_low_temp) {
		this.alarm_low_temp = alarm_low_temp;
	}
	public boolean isHwflow_enable() {
		return hwflow_enable;
	}
	public void setHwflow_enable(boolean hwflow_enable) {
		this.hwflow_enable = hwflow_enable;
	}
	public int getHwflow_query_interval() {
		return hwflow_query_interval;
	}
	public void setHwflow_query_interval(int hwflow_query_interval) {
		this.hwflow_query_interval = hwflow_query_interval;
	}
	

}
