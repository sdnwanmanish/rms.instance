package com.vz.rms.instances.rmsInstances.model;

public class SwitchingAsciInfo {
	
	private String vendor;
	private String description;
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
