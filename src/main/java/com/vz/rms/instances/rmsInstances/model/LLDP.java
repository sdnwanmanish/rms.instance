package com.vz.rms.instances.rmsInstances.model;

public class LLDP {
	
	private Integer lldp_msg_tx_hold;
	private Integer lldp_msg_tx_interval;
	private Integer lldp_reinit_delay;
	private Integer lldp_tx_delay;
	private Boolean global_lldp_enable;
	private String fb_br;
	private String fb_ip;
	public Integer getLldp_msg_tx_hold() {
		return lldp_msg_tx_hold;
	}
	public void setLldp_msg_tx_hold(Integer lldp_msg_tx_hold) {
		this.lldp_msg_tx_hold = lldp_msg_tx_hold;
	}
	public Integer getLldp_msg_tx_interval() {
		return lldp_msg_tx_interval;
	}
	public void setLldp_msg_tx_interval(Integer lldp_msg_tx_interval) {
		this.lldp_msg_tx_interval = lldp_msg_tx_interval;
	}
	public Integer getLldp_reinit_delay() {
		return lldp_reinit_delay;
	}
	public void setLldp_reinit_delay(Integer lldp_reinit_delay) {
		this.lldp_reinit_delay = lldp_reinit_delay;
	}
	public Integer getLldp_tx_delay() {
		return lldp_tx_delay;
	}
	public void setLldp_tx_delay(Integer lldp_tx_delay) {
		this.lldp_tx_delay = lldp_tx_delay;
	}
	
	public Boolean getGlobal_lldp_enable() {
		return global_lldp_enable;
	}
	public void setGlobal_lldp_enable(Boolean global_lldp_enable) {
		this.global_lldp_enable = global_lldp_enable;
	}
	public String getFb_br() {
		return fb_br;
	}
	public void setFb_br(String fb_br) {
		this.fb_br = fb_br;
	}
	public String getFb_ip() {
		return fb_ip;
	}
	public void setFb_ip(String fb_ip) {
		this.fb_ip = fb_ip;
	}
	
	
	
	

}
