package com.vz.rms.instances.rmsInstances;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;



@EnableAsync
@SpringBootApplication
@EnableTransactionManagement
@ComponentScan(basePackages = { "com.vz.rms", "com.vz.uiam" })

public class RMSApplication {

    public static void main(String[] args) {
	SpringApplication.run(RMSApplication.class, args);
    }

}