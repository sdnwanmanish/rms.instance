package com.vz.rms.instances.rmsInstances.model;

public class TcamRequestBody {
	
	private String match_mode_name;
	private int priority_low;

	private int priority_high;

	public String getMatch_mode_name() {
		return match_mode_name;
	}

	public void setMatch_mode_name(String match_mode_name) {
		this.match_mode_name = match_mode_name;
	}

	public int getPriority_low() {
		return priority_low;
	}

	public void setPriority_low(int priority_low) {
		this.priority_low = priority_low;
	}

	public int getPriority_high() {
		return priority_high;
	}

	public void setPriority_high(int priority_high) {
		this.priority_high = priority_high;
	}


}
