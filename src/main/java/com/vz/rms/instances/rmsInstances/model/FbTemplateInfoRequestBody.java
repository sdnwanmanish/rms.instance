package com.vz.rms.instances.rmsInstances.model;

public class FbTemplateInfoRequestBody {
	
	private TemplateInfo templateInfo;
	private String manufacturer;
	private String model;
	private String orderablePartNo;
	private String description;
	private String clei;
	private String materialId;
	private String weight;
	private String airFlow;
	private String powerSupplyType;	
	private String powerSupplyModelInfo;
	private String minActivePowerSupplie;	
	private String fanModulemodel;
	private String minActiveFan;
	private String maxHeatDissipation;
	private CpuInfo cupInfo;
	private SwitchingAsciInfo switchingAsciInfo;
	private String memory;
	private String flashDiskSize;
	private String hardDiscSize;
	private String nebsComplaints;	
	private DataPlaneInterfaceInfo dataPlaneInterfaceInfo;
	private EthernetManagementInterfacesInfo ethernetManagementInterfacesInfo;
	private ConsoleInterfaceInfo consoleInterfaceInfo;
	private UsbPortInfo usbPortInfo;
	private String operatingSystem;
	private String supportedTransceiver;
	private String uploadFile;
	public TemplateInfo getTemplateInfo() {
		return templateInfo;
	}
	public void setTemplateInfo(TemplateInfo templateInfo) {
		this.templateInfo = templateInfo;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getOrderablePartNo() {
		return orderablePartNo;
	}
	public void setOrderablePartNo(String orderablePartNo) {
		this.orderablePartNo = orderablePartNo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getClei() {
		return clei;
	}
	public void setClei(String clei) {
		this.clei = clei;
	}
	public String getMaterialId() {
		return materialId;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getAirFlow() {
		return airFlow;
	}
	public void setAirFlow(String airFlow) {
		this.airFlow = airFlow;
	}
	public String getPowerSupplyType() {
		return powerSupplyType;
	}
	public void setPowerSupplyType(String powerSupplyType) {
		this.powerSupplyType = powerSupplyType;
	}
	public String getPowerSupplyModelInfo() {
		return powerSupplyModelInfo;
	}
	public void setPowerSupplyModelInfo(String powerSupplyModelInfo) {
		this.powerSupplyModelInfo = powerSupplyModelInfo;
	}
	public String getMinActivePowerSupplie() {
		return minActivePowerSupplie;
	}
	public void setMinActivePowerSupplie(String minActivePowerSupplie) {
		this.minActivePowerSupplie = minActivePowerSupplie;
	}
	public String getFanModulemodel() {
		return fanModulemodel;
	}
	public void setFanModulemodel(String fanModulemodel) {
		this.fanModulemodel = fanModulemodel;
	}
	public String getMinActiveFan() {
		return minActiveFan;
	}
	public void setMinActiveFan(String minActiveFan) {
		this.minActiveFan = minActiveFan;
	}
	public String getMaxHeatDissipation() {
		return maxHeatDissipation;
	}
	public void setMaxHeatDissipation(String maxHeatDissipation) {
		this.maxHeatDissipation = maxHeatDissipation;
	}
	public CpuInfo getCupInfo() {
		return cupInfo;
	}
	public void setCupInfo(CpuInfo cupInfo) {
		this.cupInfo = cupInfo;
	}
	public SwitchingAsciInfo getSwitchingAsciInfo() {
		return switchingAsciInfo;
	}
	public void setSwitchingAsciInfo(SwitchingAsciInfo switchingAsciInfo) {
		this.switchingAsciInfo = switchingAsciInfo;
	}
	public String getMemory() {
		return memory;
	}
	public void setMemory(String memory) {
		this.memory = memory;
	}
	public String getFlashDiskSize() {
		return flashDiskSize;
	}
	public void setFlashDiskSize(String flashDiskSize) {
		this.flashDiskSize = flashDiskSize;
	}
	public String getHardDiscSize() {
		return hardDiscSize;
	}
	public void setHardDiscSize(String hardDiscSize) {
		this.hardDiscSize = hardDiscSize;
	}
	public String getNebsComplaints() {
		return nebsComplaints;
	}
	public void setNebsComplaints(String nebsComplaints) {
		this.nebsComplaints = nebsComplaints;
	}
	public DataPlaneInterfaceInfo getDataPlaneInterfaceInfo() {
		return dataPlaneInterfaceInfo;
	}
	public void setDataPlaneInterfaceInfo(DataPlaneInterfaceInfo dataPlaneInterfaceInfo) {
		this.dataPlaneInterfaceInfo = dataPlaneInterfaceInfo;
	}
	public EthernetManagementInterfacesInfo getEthernetManagementInterfacesInfo() {
		return ethernetManagementInterfacesInfo;
	}
	public void setEthernetManagementInterfacesInfo(EthernetManagementInterfacesInfo ethernetManagementInterfacesInfo) {
		this.ethernetManagementInterfacesInfo = ethernetManagementInterfacesInfo;
	}
	public ConsoleInterfaceInfo getConsoleInterfaceInfo() {
		return consoleInterfaceInfo;
	}
	public void setConsoleInterfaceInfo(ConsoleInterfaceInfo consoleInterfaceInfo) {
		this.consoleInterfaceInfo = consoleInterfaceInfo;
	}
	public UsbPortInfo getUsbPortInfo() {
		return usbPortInfo;
	}
	public void setUsbPortInfo(UsbPortInfo usbPortInfo) {
		this.usbPortInfo = usbPortInfo;
	}
	public String getOperatingSystem() {
		return operatingSystem;
	}
	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}
	public String getSupportedTransceiver() {
		return supportedTransceiver;
	}
	public void setSupportedTransceiver(String supportedTransceiver) {
		this.supportedTransceiver = supportedTransceiver;
	}
	public String getUploadFile() {
		return uploadFile;
	}
	public void setUploadFile(String uploadFile) {
		this.uploadFile = uploadFile;
	}
}
