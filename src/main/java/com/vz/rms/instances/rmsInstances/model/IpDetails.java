package com.vz.rms.instances.rmsInstances.model;

import java.io.Serializable;

public class IpDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Orchestrator orchestrator;
	private Rms rms;
	
	public Orchestrator getOrchestrator() {
		return orchestrator;
	}
	public void setOrchestrator(Orchestrator orchestrator) {
		this.orchestrator = orchestrator;
	}
	public Rms getRms() {
		return rms;
	}
	public void setRms(Rms rms) {
		this.rms = rms;
	}
	
	
	
	
	
	

}
