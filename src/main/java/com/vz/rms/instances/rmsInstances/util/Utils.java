package com.vz.rms.instances.rmsInstances.util;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.vz.rms.instances.rmsInstances.model.*;
import com.vz.rms.instances.rmsInstances.repository.RepositoryImplClass;

@Service
public class Utils {
	
	
	@Autowired
	RepositoryImplClass repositoryImplClass;
	
	@Value("${rms.serverapi.ipaddress}")
	private String serverAddress;
	
	@Value("${rms.serverapi.port}")
	private String port;
	
	private final String  rmsUrl = "/oneoss-rms/rms/v1/"; 
	
	private final String BRIDGE = "BRIDGE";
	private final String PORT = "PORT";
	private final String LLDP = "LLDP";
	
	private static final Logger logger = LoggerFactory.getLogger(Utils.class);
	
	public String getMessage(String message) throws JSONException{
		
		
		JSONObject json = new JSONObject(message);
		if(message.contains("uuid"))
		{			
			return "Uuid:"+(String) json.get("uuid");
		}
		JSONObject errorJson = json.getJSONObject("error");
		logger.info(errorJson.get("errorcode")+"-->"+errorJson.getString("error"));
		String error = "ErrorCode:"+errorJson.get("errorcode")+" Error:"+errorJson.getString("error");
		return error;
	}
	
	public Boolean getUuidFlag(String message){
		Boolean test = false;
		if(message.contains("uuid"))
		{		
			test = true;
			//return "Uuid:"+(String) json.get("uuid");
		}
		return test;
		
	}
	
	public Map<String,String> createComposite(String fbName) throws InterruptedException, ExecutionException, TimeoutException, IOException, JSONException{
		Map<String,String> message = new HashMap<String,String>();
		Bridge bridge=null;
		RMSRequestBody rMSRequestBody = new RMSRequestBody();
		Map<String,Object>mapBridge = repositoryImplClass.getAllPort(BRIDGE,fbName);
		for (String key: mapBridge.keySet()) {
		    logger.info("Bridge Name:"+key);
		    bridge = (Bridge) mapBridge.get(key);
		}
		if(bridge!=null){
			rMSRequestBody.setName(bridge.getName());
			rMSRequestBody.setDatapath_id(bridge.getDatapath_id());
			rMSRequestBody.setDatapath_type(bridge.getDatapath_type());
			rMSRequestBody.setProtocols(bridge.getProtocols());
			rMSRequestBody.setFb_ip(bridge.getFb_ip());			
		}
		LLDP lLDP =(LLDP) repositoryImplClass.get(LLDP, fbName);
		if(lLDP!=null){
			rMSRequestBody.setLldp_msg_tx_hold(lLDP.getLldp_msg_tx_hold().toString());
			rMSRequestBody.setLldp_msg_tx_interval(lLDP.getLldp_msg_tx_interval().toString());
			rMSRequestBody.setLldp_reinit_delay(lLDP.getLldp_reinit_delay().toString());
			rMSRequestBody.setLldp_tx_delay(lLDP.getLldp_tx_delay().toString());
			rMSRequestBody.setFb_ip(lLDP.getFb_ip());
			rMSRequestBody.setGlobal_lldp_enable(lLDP.getGlobal_lldp_enable());			
		}
		Map<String,Object>listPort = repositoryImplClass.getAllPort(PORT,fbName);
		System.out.println(listPort);
		if (listPort != null) {
			List<Port> listPorts = new ArrayList<Port>();
			for (String key : listPort.keySet()) {
				logger.info("Port Name:" + key);
				PortStatus portStatus = (PortStatus) listPort.get(key);
				listPorts.add(portStatus.getPort());
			}
			if (listPorts != null)
				rMSRequestBody.setPorts(listPorts);
		}
		SSLRequestBody ssl = (SSLRequestBody) repositoryImplClass.get("SSL", fbName);
		if(ssl!=null){
			rMSRequestBody.setSsl_name(ssl.getName());
			rMSRequestBody.setPrivate_key(ssl.getPrivate_key());
			rMSRequestBody.setCertificate(ssl.getCertificate());
			rMSRequestBody.setBootstrap_ca_cert(ssl.getBootstrap_ca_cert());
			rMSRequestBody.setCa_cert(ssl.getCa_cert());			
		}
		PollingFrequencyRequestBody pollingfrequency = (PollingFrequencyRequestBody) repositoryImplClass.get("POLLING", fbName);
		if(pollingfrequency!=null){
			rMSRequestBody.setCounter_query_interval(pollingfrequency.getCounter_query_interval());
			rMSRequestBody.setSfp_query_interval(pollingfrequency.getSfp_query_interval());
			rMSRequestBody.setSfp_enable(pollingfrequency.isSfp_enable());
			rMSRequestBody.setAlarm_enable(pollingfrequency.isAlarm_enable());
			rMSRequestBody.setCounter_enable(pollingfrequency.isCounter_enable());
			rMSRequestBody.setHwflow_enable(pollingfrequency.isHwflow_enable());
			rMSRequestBody.setHwflow_query_interval(pollingfrequency.getHwflow_query_interval());
			rMSRequestBody.setAlarm_high_temp(pollingfrequency.getAlarm_high_temp());
			rMSRequestBody.setAlarm_low_temp(pollingfrequency.getAlarm_low_temp());			
		}
		//Controller
		Map<String, Object> listController = repositoryImplClass.getAllPort("CONTROLLER", fbName);
		if (listController != null) {
			List<Controller> Controllers = new ArrayList<Controller>();
			for (String key : listController.keySet()) {
				logger.info("Controller Name:" + key);
				Controllers.add((Controller) listController.get(key));
			}
			if (Controllers != null)
				rMSRequestBody.setControllers(Controllers);
		}
		//lldp perint
		
		LLDPPerInterfaceRequestBody lldpint = (LLDPPerInterfaceRequestBody) repositoryImplClass.get("PERINT", fbName);
		if (lldpint != null) {
			HashMap<String, String> perInt = lldpint.getPer_interface_settings();
			List<LLDPInterfaceSettings> listLLDPInterfaceSettings = new ArrayList<LLDPInterfaceSettings>();
			for (String key : perInt.keySet()) {
				LLDPInterfaceSettings lLDPInterfaceSettings = new LLDPInterfaceSettings();
				lLDPInterfaceSettings.setName(key);
				lLDPInterfaceSettings.setLldp_enable(perInt.get(key));
				listLLDPInterfaceSettings.add(lLDPInterfaceSettings);
				logger.info("Controller Name:" + key);
			}
			if (listLLDPInterfaceSettings != null) {
				rMSRequestBody.setLldp_interface_settngs(listLLDPInterfaceSettings);
			}
		}
		ConfigureARPAndND configureARPAndND = (ConfigureARPAndND) repositoryImplClass.get("ARPND", fbName);
		if(configureARPAndND!=null){
			rMSRequestBody.setArp_subnet(configureARPAndND.getArp_subnet());
			rMSRequestBody.setNd_subnet(configureARPAndND.getNd_subnet());
		}
		//Pushing to rms
		Map<String,String> ipDetails = (Map<String,String>) repositoryImplClass.getLoginInfo("IpDetails");
		serverAddress = ipDetails.get("rmsIpAddress");
		port = ipDetails.get("rmsPort");		
		String url = serverAddress +":"+ port+rmsUrl+fbName+"/setup";
		logger.info("Url:"+url);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		String rMsString = mapper.writeValueAsString(rMSRequestBody);
		logger.info("Sent data to server for configuration:"+rMsString);
		String response = PushToRms(url, rMsString).get(20, TimeUnit.SECONDS);
		logger.info("Response:"+response);
		message.put("type", "success");
		message.put("message", "sucessfully Pushed to server!");		
		return message;
	}
	
	
	public Map<String,String> saveBridge(String fbname, Bridge bridge){
		Boolean status = false;
		Map<String,String> message = new HashMap<String,String>();
		logger.info("**AddPort**");
		
		Map<String,Object>listBridge = repositoryImplClass.getAllPort(BRIDGE,fbname);
		if(listBridge==null)
			listBridge = new HashMap<String,Object>();
		status = listBridge.containsKey(bridge.getName());
		if(status){
			logger.info("Bidge already exists!");
			message.put("type", "failure");
			message.put("message", "Bridge already exists!");
			return message;
		}
		listBridge.put(bridge.getName(), bridge);		
		repositoryImplClass.save(BRIDGE,fbname, listBridge);
		logger.info("Bridge added sucessfully!");
		message.put("type", "success");
		message.put("message", "Bridge added sucessfully!");
		return message;
	}
	public Map<String,String> saveController(String fbname, Controller controller){
		Boolean status = false;
		Map<String,String> message = new HashMap<String,String>();
		logger.info("**Save Controller**");		
		Map<String,Object>listController = repositoryImplClass.getAllPort("CONTROLLER",fbname);
		if(listController==null)
			listController = new HashMap<String,Object>();
		status = listController.containsKey(controller.getName());
		if(status){
			logger.info("Bidge already exists!");
			message.put("type", "failure");
			message.put("message", "Controller already exists!");
			return message;
		}
		listController.put(controller.getName(), controller);		
		repositoryImplClass.save("CONTROLLER",fbname, listController);
		logger.info("Controller added sucessfully!");
		message.put("type", "success");
		message.put("message", "Controller added sucessfully!");
		return message;
	}
	
	public Map<String,Object> getBridge(String fbname){
		//List<Bridge> listBridge = new ArrayList<Bridge>();		
		Map<String,Object> listBridge = repositoryImplClass.getAllPort(BRIDGE,fbname);		
		return listBridge;
		
	}
	
	@Async
	public Future<String> addBridgeInRms(String fbname,Bridge bridge) throws InterruptedException, ExecutionException, TimeoutException, IOException, JSONException{
		Map<String,String> ipDetails = (Map<String,String>) repositoryImplClass.getLoginInfo("IpDetails");
		serverAddress = ipDetails.get("rmsIpAddress");
		port = ipDetails.get("rmsPort");		
		String url = serverAddress +":"+ port+rmsUrl+fbname+"/add-bridge";
		logger.info("Url:"+url);
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		String bridgeString = mapper.writeValueAsString(bridge);
		logger.info("Sent data to server for configuration:"+bridgeString);
		String response = PushToRms(url, bridgeString).get(20, TimeUnit.SECONDS);
		logger.info("Response:"+response);		
		return new AsyncResult<String>(response);
		
	}
	@Async
	public Future<String> deleteBridgeInRms(String fbname, String brname){
		
		Map<String,String> ipDetails = (Map<String,String>) repositoryImplClass.getLoginInfo("IpDetails");
		serverAddress = ipDetails.get("rmsIpAddress");
		port = ipDetails.get("rmsPort");
		
		logger.info("**deletePort**");
		RestTemplate restTemplate = new RestTemplate();

		// HttpEntity<String> requestUpdate = new HttpEntity<String>();
		String url = serverAddress + ":" + port + rmsUrl + fbname
				+ "delete-bridge/" + brname;
		logger.info("Url" + url);
		// restTemplate.delete(url);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>("Delete FB", headers);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url,
				HttpMethod.DELETE, entity, String.class);
		logger.info("--->" + responseEntity.getBody());
		return new AsyncResult<String>(responseEntity.getBody());		
	}
	
	@Async
	public Future<String> addPortInRms(String fbname, Port portRms) throws InterruptedException, ExecutionException, TimeoutException, IOException, JSONException{
		Map<String,String> ipDetails = (Map<String,String>) repositoryImplClass.getLoginInfo("IpDetails");
		serverAddress = ipDetails.get("rmsIpAddress");
		String port1 = ipDetails.get("rmsPort");		 
		String url = serverAddress +":"+ port1+rmsUrl+fbname+"/port/add"; 
		logger.info("Url:"+url);		
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		String portString = mapper.writeValueAsString(portRms);
		String response = PushToRms(url, portString).get(20, TimeUnit.SECONDS);
		return new AsyncResult<String>(response);
	}
	
	@Async
	public Future<String> changeStatusOfPortInRms(String fbname, String portName, String status) {

		Map<String, String> ipDetails = (Map<String, String>) repositoryImplClass.getLoginInfo("IpDetails");
		serverAddress = ipDetails.get("rmsIpAddress");
		String port = ipDetails.get("rmsPort");
		logger.info("**ActivateInterface**");
		RestTemplate restTemplate = new RestTemplate();
		String url = serverAddress + ":" + port + rmsUrl + fbname + "/" + portName + "/configure/status/" + status;
		logger.info("Url:" + url);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>("Activate Interface", headers);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class);
		logger.info("Response:" + responseEntity.getBody());
		return new AsyncResult<String>(responseEntity.getBody());
	}
    @Async
    public Future<String> deletePortInRms(String fbname, String portname){
		
    	logger.info("**deletePort**");
		Map<String,String> ipDetails = (Map<String,String>) repositoryImplClass.getLoginInfo("IpDetails");
		serverAddress = ipDetails.get("rmsIpAddress");
		String port = ipDetails.get("rmsPort");
		RestTemplate restTemplate = new RestTemplate();
		String url = serverAddress + ":" + port + rmsUrl + fbname + "/"
				+ portname + "/delete";
		logger.info("Url" + url);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>("Delete Port",
				headers);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url,
				HttpMethod.DELETE, entity, String.class);
		logger.info("Response:" + responseEntity.getBody());
		return new AsyncResult<String>(responseEntity.getBody());
    }
	
	@Async
	public Future<Map<String,String>> deletePort(String fbName, String portName){	
		
		Map<String,String> message = new HashMap<String,String>();

		Boolean status = false;
		logger.info("**AddPort**");

		Map<String, Object> listPort = repositoryImplClass.getAllPort(
				PORT, fbName);
		if (listPort == null)
			listPort = new HashMap<String, Object>();
		status = listPort.containsKey(portName);
		if (!status) {
			message.put("type", "failure");
			message.put("message", "Port Not Available!");
			return new AsyncResult <Map<String, String>> (message);
		}
		listPort.remove(portName);
		repositoryImplClass.save(PORT, fbName, listPort);
		message.put("type", "success");
		message.put("message", "Port Deleted!");		
		return new AsyncResult <Map<String, String>> (message);
		
		
	}
	
	@Async
	public Future<Map<String,String>> deleteBridge(String fbname, String bridgeName){
		Boolean status = false;
		Map<String,String> message = new HashMap<String,String>();
		logger.info("**AddPort**");
		
		Map<String,Object>listBridge = repositoryImplClass.getAllPort(BRIDGE,fbname);
		if(listBridge==null)
			listBridge = new HashMap<String,Object>();
		status = listBridge.containsKey(bridgeName);
		if(!status){
			logger.info("Bidge already exists!");
			message.put("type", "failure");
			message.put("message", "Bridge Not Available!");
			return new AsyncResult <Map<String, String>> (message);
		}
		listBridge.remove(bridgeName);		
		repositoryImplClass.save(BRIDGE,fbname, listBridge);
		logger.info("Bridge deleted sucessfully!");
		message.put("type", "success");
		message.put("message", "Bridge deleted sucessfully!");
		return new AsyncResult <Map<String, String>> (message);
	}	
	@Async
	public Future<String> PushToRms(String url, String data) throws IOException, JSONException {

		logger.info("URL:" + url + " ,Data:" + data);
		URL object = new URL(url);
		HttpURLConnection con = (HttpURLConnection) object.openConnection();
		//String encoding = Base64.getEncoder().encodeToString("admin:admin".getBytes());
		con.setDoOutput(true);
		//con.setRequestProperty("Authorization", "Basic " + encoding);
		con.setDoOutput(true);
		con.setDoInput(true);
		con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		con.setRequestMethod("POST");
		OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		wr.write(data);
		wr.flush();
		StringBuilder sb = new StringBuilder();
		int HttpResult = con.getResponseCode();
		logger.info("HttpResult:" + HttpResult);
		BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
		String line = null;
		while ((line = br.readLine()) != null) {
			sb.append(line + "\n");
		}
		br.close();
		logger.info("Message From Server:" + sb.toString());
		return new AsyncResult<String>(sb.toString());

	}
	
}
