package com.vz.rms.instances.rmsInstances.model;

import java.io.Serializable;
import java.util.List;

public class ConfigureARPAndND implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<String> arp_subnet;
	private List<String> nd_subnet;
	
	public List<String> getArp_subnet() {
		return arp_subnet;
	}
	public void setArp_subnet(List<String> arp_subnet) {
		this.arp_subnet = arp_subnet;
	}
	public List<String> getNd_subnet() {
		return nd_subnet;
	}
	public void setNd_subnet(List<String> nd_subnet) {
		this.nd_subnet = nd_subnet;
	}
	
	
	

}
