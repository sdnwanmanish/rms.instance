package com.vz.rms.instances.rmsInstances.model;

public class UsbPortInfo {
	
	private String usbInterfaceLabel;

	public String getUsbInterfaceLabel() {
		return usbInterfaceLabel;
	}

	public void setUsbInterfaceLabel(String usbInterfaceLabel) {
		this.usbInterfaceLabel = usbInterfaceLabel;
	}
	

}
