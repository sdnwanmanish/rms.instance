package com.vz.rms.instances.rmsInstances.model;

import java.util.List;

public class ListPort {
	
	private List<Interface> interfaces;
	private Integer count;
	public List<Interface> getInterfaces() {
		return interfaces;
	}
	public void setInterfaces(List<Interface> interfaces) {
		this.interfaces = interfaces;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	
	

}
