package com.vz.rms.instances.rmsInstances.model;

public class ConsoleInterfaceInfo {
	
	private String consoleLabel;
	private String connectionType;
	public String getConsoleLabel() {
		return consoleLabel;
	}
	public void setConsoleLabel(String consoleLabel) {
		this.consoleLabel = consoleLabel;
	}
	public String getConnectionType() {
		return connectionType;
	}
	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}
	
	

}
