package com.vz.rms.instances.rmsInstances.model;

import java.io.Serializable;

public class SSLRequestBody implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String private_key;
	private String certificate;
	private String ca_cert;
	private String bootstrap_ca_cert;
	private String fb_ip;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrivate_key() {
		return private_key;
	}
	public void setPrivate_key(String private_key) {
		this.private_key = private_key;
	}
	public String getCertificate() {
		return certificate;
	}
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	public String getCa_cert() {
		return ca_cert;
	}
	public void setCa_cert(String ca_cert) {
		this.ca_cert = ca_cert;
	}
	public String getBootstrap_ca_cert() {
		return bootstrap_ca_cert;
	}
	public void setBootstrap_ca_cert(String bootstrap_ca_cert) {
		this.bootstrap_ca_cert = bootstrap_ca_cert;
	}
	public String getFb_ip() {
		return fb_ip;
	}
	public void setFb_ip(String fb_ip) {
		this.fb_ip = fb_ip;
	}
	
	
	
	
}
