package com.vz.rms.instances.rmsInstances.model;

import java.io.Serializable;
import java.util.HashMap;

public class LLDPPerInterfaceRequestBody implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fb_br;
	private String fb_ip;

	private HashMap<String,String> per_interface_settings;

	public String getFb_br() {
		return fb_br;
	}

	public void setFb_br(String fb_br) {
		this.fb_br = fb_br;
	}

	public String getFb_ip() {
		return fb_ip;
	}

	public void setFb_ip(String fb_ip) {
		this.fb_ip = fb_ip;
	}

	public HashMap<String, String> getPer_interface_settings() {
		return per_interface_settings;
	}

	public void setPer_interface_settings(HashMap<String, String> per_interface_settings) {
		this.per_interface_settings = per_interface_settings;
	}

}
